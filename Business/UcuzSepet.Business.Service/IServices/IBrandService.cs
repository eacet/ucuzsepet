﻿using UcuzSepet.Data.Domain.Entities;
using UcuzSepet.Data.EF.Components;

namespace UcuzSepet.Business.Service.IServices {

    /// <summary>
    /// Brand Service for Access Brand entities
    /// </summary>
    public interface IBrandService : IBaseService<Brand> {
    }
}
