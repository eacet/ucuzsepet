﻿using UcuzSepet.Data.Domain.Entities;
using UcuzSepet.Data.EF.Components;

namespace UcuzSepet.Business.Service.IServices {
    /// <summary>
    /// Category Service for Access Category entities
    /// </summary>
    public interface ICategoryService : IBaseService<Category> {
    }
}
