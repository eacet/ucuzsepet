﻿using UcuzSepet.Data.Domain.Entities;
using UcuzSepet.Data.EF.Components;

namespace UcuzSepet.Data.EF.IRepositories {
    /// <summary>
    /// Category Repository for Access Category Entities
    /// </summary>
    public interface ICategoryRepository : IEFBaseRepository<Category> {
    }
}
