﻿using UcuzSepet.Data.Domain.Entities;
using UcuzSepet.Data.EF.Components;

namespace UcuzSepet.Data.EF.IRepositories {
    /// <summary>
    /// Brand Repository for Access Brand Entities
    /// </summary>
    public interface IBrandRepository : IEFBaseRepository<Brand> {
    }
}
